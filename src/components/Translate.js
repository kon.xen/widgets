import React,{ useState,  useEffect, useRef } from "react";
import Dropdown from "./Dropdown";
import Convert from './Convert';

//key:  AIzaSyCHUCmpR7cT_yDFHC98CZJy2LTms-IwDlM
const options = [
    {
        label: 'Africaans',
        value: 'af'
    },
    {
        label: 'Arabic',
        value: 'ar'
    },
    {
        label: 'Hindi',
        value: 'hi'
    },
    {
        label: 'Hellenic',
        value: 'el'
    },
    {
        label: 'Polish',
        value: 'pl'
    },
    {
        label: 'Portuguese',
        value: 'pt'
    },
    {
        label: 'English',
        value: 'en'
    }
];

const Translate = () => {
    const [language, setLanguage] = useState( options[0] );
    const [text, setText] = useState('');

    return (
        <div>
            <div className="ui form">
                <label htmlFor="">Enter text</label>
                <div className="field">
                    <input value={ text }
                           onChange={(e)=> setText(e.target.value)}
                    />
                </div>
            </div>
            <Dropdown
                label='Select a language'
                selected={language}
                onSelectedChange={ setLanguage }
                options={ options }
            />
            <hr/>
            <h3 className="ui header">Output</h3>
            <Convert language={language} text={text}/>
        </div>
    );

}

export default Translate;